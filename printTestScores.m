clc
load('philConceptsStat.mat');

fprintf('\n Initial number of impostors: %g\n',model{1,1}.initialNumImpostors);


%% Different values of lambda used for simualtion
lambdaVec = power(10, -6:4);
lambdaVec = lambdaVec(:);
numParams = length(lambdaVec);


for m = 1:2
    fprintf('\n%s\n',model{m,1}.algo);

 
    if(m>1) % If not Euclidean
        limit = numParams;
    else
        limit = 1; % If Euclidean just one run is fine
    end
    
    for s = 1:limit
        if(m>1)
            fprintf('\nLambda : %f\n',model{m,s}.params.lambda)
        end
        
        % Computing the average AUC, AUC, True Positive and False
        % positive rate
        [avgAUC(m,s), auc{m,s}, truePos{m,s}, falsePos{m,s}] = tieredLinkROC(data, model{m,s}); 
        fprintf('Average AUC score %f\n\n', avgAUC(m,s));
        %}
        % Print the Spearman's footrule
%         fprintf('Initial Spearman''s foot-rule score %f\n', model{m,s}.initialSpearScore);
        fprintf('Final Spearman''s foot-rule score %f\n', model{m,s}.finalSpearScore);        
        % Print final number of impostors
%         model{m,s}.afterNumImpostors = countNumImpostors(data, model{m,s});
        fprintf('Final number of impostors: %g\n',model{m,s}.afterNumImpostors);        
%         fprintf('Norm of the metric M %f\n', norm(full(model{m,s}.params.M),2));
        %{ 
        % This code is commented since it requires more testing
        %  Compute the Stochastic Inversion sampling
        M = model{m,s}.params.M;
        model{m,s}.stochInvScore = stochInvSampling(X' * M * X);
        fprintf('\n Stochastic Inversion score : %g\n', model{m,s}.stochInvScore);
        %}
    end   
end