function score = compareDistances(data, model)
% Compute average Spearman's footrule between distance rankings

X = data.X;
A = data.A;
M = model.params.M;
% assumes that A is symmetric

C = computeCommuteTime(A);
% C = computeHittingTime(A);

D = computeMetricDistance(X, M);

n = size(A,1);

% indexMatrix = triu(true(n, n));

% compute Spearman's rho correlation between distances
% score = corr(C(indexMatrix), D(indexMatrix), 'type', 'Spearman');

% compute Spearman's footrule between distance rankings
% [~, graphRank] = sort(C(indexMatrix));
% [~, metricRank] = sort(D(indexMatrix));

% score = sum(abs(graphRank - metricRank));


% compute average Spearman's footrule between distance rankings
[~, graphRank] = sort(C, 1);
[~, metricRank] = sort(D, 1);

score = sum(abs(graphRank(:) - metricRank(:)) / (n^3 / 3));