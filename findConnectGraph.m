function [truncData, visited] = findConnectGraph(varargin)
%FINDCONNECTGRAPH Summary of this function goes here
%   This function finds the largest connected component of a graph using
%   the breadth-fast traversal technique. It can also compute the k-hop
%   neighbors of all the nodes.


data = varargin{1};
A = data.A;
X = data.X;
N = size(A,1); % Number of nodes in the graph

if(nargin<2)% if we do not supply the node to start from 
    [~, startNode] = max(sum(A,1)); % The most-well connected node of the graph
    hop = 6;
    C = computeCommuteTime(A);
else
    startNode = varargin{2}; % custom starting node supplied by the user

    if(nargin>2)
        hop = varargin{3};
    else% The code goes upto 6-hop neighbors. You can modify it by changing the variable 'hop'
        hop = 6;
    end
    
    if(nargin>3)
        C = varargin{4};
    else
        C = computeCommuteTime(A);
    end    

end


%% Start the breadth-first traversal from the node stored in 'startNode'
 
hopLevel = 1;
visited = struct('neighbors', startNode, 'commuteTime', []); %Holds the varying hop neighbors

visitInd = true(N,1);
visitInd(startNode) = false;% This is a reverse flag that is false corresponding to all nodes checked.


while(hopLevel<=hop)
    hopLevel = hopLevel + 1;
    tempVisitInd = false(N,1);
    prevLevel = visited(hopLevel-1,1).neighbors;
    for i = 1:numel(prevLevel)
        tempVisitInd = tempVisitInd | (A(:,prevLevel(i)) & visitInd);
    end
    levelK = find(tempVisitInd);
    visited = [visited;...
        struct('neighbors', levelK, 'commuteTime', C(levelK, startNode))];
    visitInd(visited(hopLevel).neighbors) = false;

end

visited = visited(2:end);

% Truncate the Adjacency matrix A and the attribute matrix X so that only
% the connected components remain
truncData = data;
truncData.X = X(:, ~visitInd);
truncData.A = A(~visitInd, ~visitInd);
truncData.N = size(truncData.A,1);


end