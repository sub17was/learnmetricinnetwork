function Katz = computeKatzMeasure(A)

% Computes the Katz measure for a connected graph encoded in adjacency
% matrix A

% keeps a cache of the last-used A

persistent savedA
persistent savedKatz;

n = size(A,1);

if ~isempty(savedA) && all(size(savedA) == size(A)) && all(savedA(:) == A(:))
    Katz = savedKatz;
else
    % alpha should be less than the inverse of largest singular value of the 
    % matrix A as per classical formula mentioned in David Gleich's work 
    % https://www.cs.purdue.edu/homes/dgleich/publications/Bonchi%202012%20-%20fast%20katz.pdf
    A = full(A);    
    singularVals = svds(A);
    alpha = 0.5/singularVals(1);
    Katz = inv(eye(n) - alpha * A) - eye(n);
        
    savedA = A;
    savedKatz = Katz;
end


