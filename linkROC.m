function [auc, truePos, falsePos] = linkROC(X, A, M)

% measure area under ROC curve for link prediction based on
% distance

n = size(A,1);

if n > 20000
    fprintf('The %d x %d distance matrix is probably too big to compute. Aborting...\n', n, n);
    return;
end

K = X'*M*X;
XX = diag(K);

D = full(XX * ones(1,n) + ones(n, 1) * XX' - 2 * K);

[truePos, falsePos, thresholds, auc] = perfcurve(full(A(:)), -D(:), 'true', 'xCrit', 'TPR', 'yCrit', 'FPR');
