function params = checkParams(data, params)
%DEFAULTPARAMS Summary of this function goes here
%   Detailed explanation goes here


%% set params if not set
if ~isfield(params, 'lambda')
    params.lambda = 1e-4;
end

if ~isfield(params, 'maxNumMinutes')
    params.maxNumMinutes = 7*24*60;
end

if ~isfield(params, 'maxIter')
    params.maxIter = 1e4;
end

if ~isfield(params, 'margin')
    params.margin = 1e-2;
end

if ~isfield(params, 'miniBatchSize')
    params.miniBatchSize = 10;
end

if ~isfield(params, 'diagonal')
    params.diagonal = true;
end

if ~isfield(params, 'printEvery')
    params.printEvery = 0;
end

if ~isfield(params, 'project')
    params.project = 'final';
end

if ~isfield(params, 'maxDepth')
    params.maxDepth = 3;
end
% maximum size graph that we still count imposters on
if ~isfield(params, 'MAX_COUNT_IMPOSTERS')
    params.MAX_COUNT_IMPOSTERS = 1100;
end

params.M = speye(data.dim); % Identity matrix

end