function numImpostors = countNumImpostors(data, model)
% countNumImpostors
% 
% for each nodes computes the number of disconnected nodes which are closer
% than the furthest connected neighbor for that node and returns the sum of
% the impostor value over all nodes
%
% Computes the full node by node matrix, so only use on small problems
% 
% X - features for each node
% M - metric to be used, identity is euclidean
% A - Adjacency matrix

X = data.X;
A = data.A;
M = model.params.M;

% Number of nodes
N = size(A, 2);

K = X' * M * X;

% Amount of slack permitted
margin = model.params.margin;

% Computes the sum of number of impostors for all nodes
numImpostors = 0;


for i=1:N
    
    % Reference node
    idx1 = i;    
    
    ii = zeros(1, N);
    ii(idx1) = 1;
    
    % Find the nodes connected to the i-th node
    [~, conIDX] = find(A(idx1, :) == 1);
    % Find the nodes not connected to the i-th node
    [~, disIDX] = find((A(idx1, :) + ii) == 0);
    
    
    Kii = K(idx1, idx1);
    Kij = K(idx1, :);
    Kjj = diag(K)';
    
    % Distance between i-th node and all other nodes using M
    dists = Kii + Kjj - 2*Kij;
    
    % Find the attribute distance (as measured by M) to the farthest
    % directly connected node and also determine the index of the node
    [conDistance, idx] = max(dists(conIDX)); 
    
    
    if (~isempty(idx))    
        
        % Find the indices of the nodes which are not connected to the i-th
        % node and also closer than the farthest node connected to node i
        [~, idx] = find(dists(disIDX) < (conDistance + margin));

        % Add the number of such 'target impostors'
        numImpostors = numImpostors + length(idx);
    end

end