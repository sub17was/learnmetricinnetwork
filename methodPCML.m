function model = methodPCML(data, params)
%CTML Summary of this function goes here
%   Detailed explanation goes here

[D, N] = size(data.X);


iterProject = strcmp(params.project, 'iter');
finalProject = strcmp(params.project, 'final');

% maximum size graph that we still count imposters on
MAX_COUNT_IMPOSTERS = params.MAX_COUNT_IMPOSTERS;


%% load params into short variables
lambda = params.lambda;
maxNumMinutes = params.maxNumMinutes;
T = params.maxIter;
margin = params.margin;
miniBatchSize = params.miniBatchSize;


%% setup model output
model.algo = 'PCML';
model.params = params;
model.predictor = @predictorMetrics;
X = data.X;
M = params.M;


if N < MAX_COUNT_IMPOSTERS
    model.initialNumImpostors = countNumImpostors(data, model);
end
% Initial Spearman's score helps to see how well the alignment was with
% commute time initially before the learning took place
model.initialSpearScore = compareDistances(data, model);

%% Stochastic Gradient Descent applied on triplets sampled from absorbing random walk
tic
for t=1:T
    
    eta = 1/(lambda * t);    
    
    % The sparse matrix for faster computation applied
    C = sparse([], [], [], N, N, miniBatchSize*9);
    scores(t) = 0;
    
    triplets = sampleTriplet(data, miniBatchSize, 2);  % Absorbing random walk
    
    for bb = 1 : miniBatchSize
%         tripInd = randi(size(triplets,1)); % Randomly select any triplet
        tripInd = bb; % Epoch style sequential scanning
        i = triplets(tripInd, 1);
        j = triplets(tripInd, 2);
        k = triplets(tripInd, 3);
        %% faster kernel value computation
        XiM = (M * X(:,i))';
        XjM = (M * X(:,j))';
        XkM = (M * X(:,k))';
        
        Kii = XiM * X(:,i);
        Kjj = XjM * X(:,j);
        Kkk = XkM * X(:,k);
        Kij = XiM * X(:,j);
        Kji = Kij;
        Kik = XiM * X(:,k);
        Kki = Kik;
        
        % Distance to disconnected node 'k'
        distk = Kii + Kkk - Kik - Kki;
        
        % Distance to connected node 'j'
        distj = Kii + Kjj - Kij - Kji;
               
        if (distk < distj + margin)
            %% old easier to read version
            %{
                %                 C(j, j) = C(j, j) + 1;
                %                 C(i, j) = C(i, j) - 1;
                %                 C(j, i) = C(j, i) - 1;
                %                 C(i, k) = C(i, k) + 1;
                %                 C(k, i) = C(k, i) + 1;
                %                 C(k, k) = C(k, k) - 1;
            %}
            %% sparse version
            C = C + sparse([j i j i k k], [j j i k i k], [1 -1 -1 1 1 -1], N, N);            
            scores(t) = scores(t) + 1;
        end
    end   
    
    C = sparse(C);
    
    if params.diagonal
        XC = X * C;
        cols = find(sum(XC));
        dGrad = sum(XC(:,cols).*X(:,cols), 2);
        % dGrad = sum((X*C) .* X, 2);
        grad = sparse(1:D, 1:D, dGrad, D, D, D) + lambda * M;
    else
        grad = X * C * X' + lambda * M;
    end
    
    % Update the Mahalanobis matrix M
    
    if(scores(t)>0)
        M0 = M; 
        M = M0 - eta * grad;
    end
    
    
    if iterProject
        M = psdProject(M, params);
    end
    
    if (mod(t, params.printEvery) == 0)
        figure(5);
        semilogx(1:t, smooth(scores(1:t), 500, 'moving'));
        title('PCML: Number of Impostors Per Batch');
        xlabel('Iterations');
        ylabel('Number of Impostors Found');
        drawnow;
    end
    
    if (toc > maxNumMinutes*60)
        break;
    end
   %{
   if rem(t,50)==0 || t==1
        loss(floor(t/50)+1,1) = scorer(data, M, lambda);
   end
    %}
end

% semilogy([1 50:50:T],loss);

%Projecting the final Mahalanobis matrix M
if finalProject
    M = psdProject(M, params);
end
%}

% Updating the model that is returned
model.params.M = M; % The learnt M
model.numImpIts = scores; % The number of impostor seen per iteration
model.elapsedSec = toc; % Time taken to train

if N < MAX_COUNT_IMPOSTERS % The number of impostors after the learning
    model.afterNumImpostors = countNumImpostors(data, model);
end

% Spearman's foot-rule score after learning M
model.finalSpearScore = compareDistances(data, model);