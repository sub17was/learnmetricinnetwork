function graphDistance = shortestPath(A, centralNode)

% Returns the shortest-path distance from centralNode

n = size(A,1);

distance = inf(n,1);

distance(centralNode) = 0;

for current = 1 : n
    neighbors = A(:, current);
    distance(neighbors) = min(distance(neighbors), distance(current) + 1);
end

%% set up distance batches

[distances, ~, dI] = unique(distance);

distanceIndex = cell(length(distances), 1);

for i = 1:length(distances)
    distanceIndex{i} = distance == distances(i);
end

%% package computed quantities into struct

graphDistance.distance = distance;
graphDistance.distances = distances;
graphDistance.distanceIndex = distanceIndex;
graphDistance.dI = dI;

