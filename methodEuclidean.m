function model = methodEuclidean(data, params)
%METHODEUCLIDEAN Summary of this function goes here
%   Detailed explanation goes here


%% Setup model output
model.algo = 'Euclidean';
model.params = params;
M = params.M;


model.initialNumImpostors = countNumImpostors(data, model);
model.initialSpearScore = compareDistances(data, model);


% Updating the model that is returned
model.params.M = M;
model.afterNumImpostors = model.initialNumImpostors;
model.finalSpearScore = compareDistances(data, model);

end

