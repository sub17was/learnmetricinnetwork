%% Compute all-pairs shortest paths 
load data/WikiBFSDump100000;
A = A & A';
%%
allPairs = intmax('uint8') * ones(size(A,1), size(A,2), 'uint8');

allPairs(A(:)) = 1;

%%

timer = tic;

for current = 1:size(A,1)
    
    lengthThroughCurrent = bsxfun(@plus, allPairs(:,current), allPairs(current,:));
    
    allPairs = min(allPairs, lengthThroughCurrent);
        
    if mod(current, 1) == 0
        fprintf('Finished %d of %d. ETA %f minutes\n', current,...
            size(A,1), (size(A,1) - current)*(toc(timer) / current)/60);
        drawnow;
    end
end

save allPairs allPairs;
