function negInversions = evaluateInversions(X, A, M, graphDistanceComputer)

% graphDistance is a struct containing the computed distances and indexes
% of tiers of distance

%% Compute graph distance from one node

% centralNode = find(strcmp(names, 'Virginia'));
[~, centralNode] = max(sum(A));

graphDistance = graphDistanceComputer(A, centralNode);

distance = graphDistance.distance;
distances = graphDistance.distances;
dI = graphDistance.dI;

%% Compute attribute distances from node

metricDistances = full(sqrt(X(:, centralNode)'*M*X(:, centralNode) - 2*(X(:, centralNode)'*M*X)' + sum((X'*M)'.*X)'));

% compute disagreements
[~, inds] = sort(metricDistances);
inversions = 0;

tiers = zeros(length(distances), 1);

for i = 1:length(distances)
    tiers(i) = nnz(distance == distances(i));
end

cTiers = [0; cumsum(tiers)];

for i = 1:length(metricDistances)
    % number of edges that should be less than pair i
    correctRange = cTiers([dI(inds(i)), dI(inds(i))+1]);
    
    if i < min(correctRange) || i > max(correctRange)
        inversions = inversions + min(abs(i - correctRange));
    end
end

negInversions = -inversions;

