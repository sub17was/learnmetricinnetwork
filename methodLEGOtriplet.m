function model = methodLEGOtriplet(data, params)
%LEGO Summary of this function goes here
%   Detailed explanation goes here

[D, N] = size(data.X);
iterProject = strcmp(params.project, 'iter');
finalProject = strcmp(params.project, 'final');

% maximum size graph that we still count imposters on
MAX_COUNT_IMPOSTERS = 1000;


%% load params into short variables
lambda = params.lambda;
maxNumMinutes = params.maxNumMinutes;
T = params.maxIter;
margin = params.margin;
miniBatchSize = params.miniBatchSize;


%% setup model output
model.algo = 'LEGOtriplet';
model.params = params;
model.predictor = @predictorMetrics;
X = data.X;

M0 = params.M;
% Deg = degreeDistribution(A);
if N < MAX_COUNT_IMPOSTERS
    model.initialNumImpostors = countNumImpostors(data, model);
end

% Sample the triplets
triplets = sampleTriplet(data, T, 1);
% [low, up] = ComputeDistanceExtremes(X, 5, 95, speye(D));

% use validation set to tune parameter eta
% C = GetConstraints(A, T, low, up);

eta = lambda;
%% Optimizing the log-det regularizer
M = M0;
tic
for i = 1:T
    
%     triplets = sampleTripletSPML(A, N, miniBatchSize); % Random selection
%     for batch = 1 : miniBatchSize
%         tripInd = batch; % Epoch style sequential scanning
        ut = triplets(i,1);
        vt = triplets(i,2);
        wt = triplets(i,3);
        
        z1t = X(:,ut) - X(:,vt);
        z2t = X(:,ut) - X(:,wt);  
        
        zuv = z1t' * M * z1t;
        zuw = z2t' * M * z2t;
        zdiff = zuv - zuw;
        zt = max(zdiff + margin, 0); %hinge-loss
        
        if(zt>0) % If constraint is violated
            % Get a rank-1 approximation of the rank-2 matrix (z1t*z1t' - z2t*z2t')
            [eigenvector, eigenvalue] = eigs(z1t*z1t' - z2t*z2t',1);
            z = eigenvector * sqrt(eigenvalue);

            Mz = M * z;
            beta = 2*eta / (1 + 2*eta*z'*Mz);
            
            % Update the equation
            M = M - beta*(M*z)*(z'*M); 
            
        end
        
        if(rem(i,100)==0)
            disp(i);
        end
end

if max(max(isnan(M))) == 1
    M = speye(D);
end


%{
% Projecting the final Mahalanobis matrix M
if(finalProject)
    M = psdProject(M, params);
end
%}

% Updating the model that is returned
model.params.M = M;
% model.numImpIts = scores;
model.elapsedSec = toc;
model.afterNumImpostors = countNumImpostors(data, model);
model.finalSpearScore = compareDistances(data, model);
