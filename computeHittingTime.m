function H = computeHittingTime(A)

% Computes the hitting-time for a connected graph encoded in adjacency
% matrix A

% keeps a cache of the last-used A

persistent savedA
persistent savedLinv;

n = size(A,1);

if ~isempty(savedA) && all(size(savedA) == size(A)) && all(savedA(:) == A(:))
    Linv = savedLinv;
else
    % compute Laplacian
    
    A = full(A);
    d = sum(A,2);
    D = diag(d);
    L = D - A;
    
    Linv = pinv(L);
    
    savedA = A;
    savedLinv = Linv;
end

Ldiag = diag(Linv);

volume = sum(A(:));

H = volume * (Ldiag * ones(1,n) - Linv');