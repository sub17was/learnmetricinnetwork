function model = methodLEGO(data, params)
%LEGO Summary of this function goes here
%   Detailed explanation goes here

[D, N] = size(data.X);
iterProject = strcmp(params.project, 'iter');
finalProject = strcmp(params.project, 'final');




%% load params into short variables
lambda = params.lambda;
% maxNumMinutes = params.maxNumMinutes;
T = params.maxIter;
MAX_COUNT_IMPOSTERS = params.MAX_COUNT_IMPOSTERS;
% margin = params.margin;
% miniBatchSize = params.miniBatchSize;


%% setup model output
model.algo = 'LEGO';
model.params = params;
model.predictor = @predictorMetrics;
X = data.X;
A = data.A;
M0 = params.M;

% Deg = degreeDistribution(A);
if N < MAX_COUNT_IMPOSTERS
    model.initialNumImpostors = countNumImpostors(data, model);
end
model.initialSpearScore = compareDistances(data, model);


[low, up] = ComputeDistanceExtremes(X, 5, 95, speye(D));

% use validation set to tune parameter eta
C = GetConstraints(A, T, low, up);

eta = lambda;
%% Optimizing the log-det regularizer
M = M0;

for i = 1:T
	ut = C(i,1);
	vt = C(i,2);
	zt = X(:,ut) - X(:,vt);
	target_dis = C(i,4); % This is the y_t
	yt = zt'*M*zt; % This is the predicted distance or the y cap
	a = (2*eta*target_dis*yt - 1) + sqrt((2*eta*target_dis*yt-1)^2 + 8*eta*yt*yt);
	a = a / (4*eta*yt); % This is the y hat
	beta = 2*eta*(a-target_dis) / (1 + 2*eta*(a - target_dis)*yt);
    if(C(i,3) == 1 && isnan(a) == 0)
        if(target_dis < yt)
			M = M - beta*(M*zt)*(zt'*M); 
        end
    elseif(C(i,3) == -1 && isnan(a) == 0)
        if(target_dis > yt)
			M = M - beta*(M*zt)*(zt'*M); 
        end
    end
end

if max(max(isnan(M))) == 1
    M = speye(D);
end
%{
    pred_itml_val = KNN(trainy, trainX, sqrtm(M_itml), knn_size, testX);            
    acc_itml_val(j,t) = sum(pred_itml_val==testy)/length(testy);
   
% end

[yi,i1] = max(mean(acc_itml_val));
sprintf('Chosen eta for itml: %d', eta(i1))
for j = 1:10
    %50 percent training, 50 percent test
    [n,d] = size(X);
    rp = randperm(n);
    trainInd = rp(1:floor(.5*n));
    testInd = rp(floor(.5*n)+1:end);

    trainX = X(trainInd,:);
    trainy = y(trainInd);
    testX = X(testInd,:);
    testy = y(testInd);

    [l,u] = ComputeDistanceExtremes(trainX,5,95,eye(d));
    knn_size = 3;
    A_0 = eye(d);

    %Now run on full training and testing set
    C = GetConstraints(trainy,10000, l, u);
    ind = find(C(:,1) ~= C(:,2));
    C = C(ind,:);
    [c,s] = size(C);

    %Run OnlineITML
    tic;
    params.eta = eta(i1);
    A_itml = OnlineItml(C,trainX,A_0,params);
    if max(max(isnan(A_itml))) == 1
        A_itml = eye(d);
    end
    cputime_itml(j) = toc;


    %Do k-nn over test data
    pred_itml = KNN(trainy, trainX, sqrtm(A_itml), knn_size, testX);
    pred_baseline = KNN(trainy, trainX, eye(d), knn_size, testX);

    acc_itml(j) = sum(pred_itml==testy)/length(testy);
    acc_baseline(j) = sum(pred_baseline==testy) / length(testy);
end
itml(dataset) = mean(acc_itml)
baseline(dataset) = mean(acc_baseline)

itml_time(dataset) = mean(cputime_itml);

%} 

%{
 % Projecting the final Mahalanobis matrix M
if finalProject
    M = psdProject(M, params);
end
%}

% Updating the model that is returned
model.params.M = M;
model.afterNumImpostors = countNumImpostors(data, model);
model.finalSpearScore = compareDistances(data, model);



function C = GetConstraints(A, numConst, low, up)
% C = GetConstraints(A, numConst, low, up)
%
% Get distance constraint matrix from connections in A.  
% C: 4 column matrix
%   column 1, 2: index of constrained points.  Indexes between 1 and n
%      column 3: 1 if nodes are connected, -1 if disconnected
%      column 4: right-hand side (lower or upper bound, depending on 
%                   whether points are similar or dissimilar)
%

N = size(A,1);
C = zeros(numConst, 4);

for k=1:numConst
    % Generate a pair of points
    flag = true;
    while(flag)
        u = ceil(rand * N);
        neighbors = find(A(:,u));
        degree = nnz(neighbors);
        if(degree>0)
            % With probability 0.5, incorporate a neighbor as v
            if(rand<0.5)
               v = neighbors(randi(degree));
            else
                v = ceil(rand * N);
            end
            flag = (u==v);
        end
    end
    % Assign target distance
    if(A(u,v)==1) % (u,v) are connected
        C(k,:) = [u v 1 low];  
    else
        C(k,:) = [u v -1 up];
    end    
    
end






function [low, up] = ComputeDistanceExtremes(X, a, b, M)
% [l, u] = ComputeDistanceExtremes(X, a, b, M)
%
% Computes sample histogram of the distances between rows of X and returns
% the value of these distances at the a^th and b^th percentils.  This
% method is used to determine the upper and lower bounds for
% similarity / dissimilarity constraints.  
%
% X: (n x m) data matrix 
% a: lower bound percentile between 1 and 100
% b: upper bound percentile between 1 and 100
% M: Mahalanobis matrix to compute distances 
%
% Returns l: distance corresponding to a^th percentile
% u: distance corresponding the b^th percentile

if (a < 1 || a > 100),
    error('a must be between 1 and 100')
end
if (b < 1 || b > 100),
    error('b must be between 1 and 100')
end

N = size(X, 2);


%% Random trials
num_trials = min(1000, N*(N-1)/2);

% we will sample with replacement
dists = zeros(num_trials, 1);
for i = 1:num_trials
    u = ceil(rand*N);
    v = ceil(rand*N);    
    dists(i,1) = (X(:,u) - X(:,v))'*M*(X(:,u) - X(:,v));
end

%{
% From the full matrix
dists = zeros(N*(N-1)/2,1);
K = X'*M*X;
XX = diag(K);
D = full(XX * ones(1,N) + ones(N, 1) * XX' - 2 * K);
start = 1;
for i = 1:N
    dists(start:start+N-i) = D(i:end,i);
    start = start+N-i+1;
end
%}
% Select the a-th and b-th percentile values as 'low' and 'high'
[~, c] = hist(dists, 100);
low = c(floor(a));
up = c(floor(b));