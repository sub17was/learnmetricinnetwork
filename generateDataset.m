clear
rng(1);

n = 100;
% [~, A] = preferential(d,n);

A = rand(n) < 0.4;
A = A & A';
A = A - sparse(1:n, 1:n, diag(A)); % remove self-links

% nnz(A)
% data = struct('X',rand(d,n),'A',A);
% data = findConnectGraph(data);

% X = full(data.X);
% A = full(data.A);
D = diag(sum(A,2)); % degree matrix
L = D - A; % laplacian of the graph

volume = nnz(A); % volume (number of edges) of the graph

Lpinv = pinv(L); % Pseudo-inverse of the graph Laplacian
K2 = volume * Lpinv; % Kernel matrix for commute time embeddings

% M = psdProject(K2, []);
[eigenvectors,eigenvalues] = eigs(K2, n-1); %Eigen-decomposition of kernel

X = sqrt(eigenvalues)*eigenvectors'; % Generated feature vector set
d = sum(size(X)) - n; % Number of dimensions of the dataset

K1 = X'*X; % Polynomial kernel matrix embedding the dot-product of features


% ComTime = diag(Lpinv)*ones(1,n) + ones(n,1)*diag(Lpinv)' - Lpinv - Lpinv';
ComTime = diag(K2)*ones(1,n) + ones(n,1)*diag(K2)' - K2 - K2';
EuclidDist = diag(K1)*ones(1,n) + ones(n,1)*diag(K1)' - K1 - K1';

% Seeing the error values help in cross-checking the generated matrices
residualKernels = norm(K1 - K2,2)
residualEuclidComms = norm(ComTime - EuclidDist,2)
residualEuclids = norm(power(dist(X),2) - EuclidDist,2)
residualComTimes = norm(ComTime - computeCommuteTime(A),2)


[~, graphRank] = sort(ComTime, 1); % Sorting nodes based on commute time
[~, metricRank] = sort(EuclidDist, 1); % Sorting nodes based on attribute distance

rankMismatch = abs(graphRank(:) - metricRank(:)); % computing the mismatches
score = sum(rankMismatch)/ (n^3 / 3) % To see if metric ranks aligns with graph rank

data = struct('X', X, 'A', A, 'L', L, 'dim', d, 'D',D, 'N', n, 'C', ComTime,...
    'Katz',computeKatzMeasure(A)); % storing the 'data' structure

% Computing the diameter of the graph. If diamemter is D, the A^D has all
% non-zero entries for a connected graph
Ahop = A;
hop = 0;
while(nnz(Ahop)<n*n)
   Ahop = Ahop*A; 
   hop = hop+1;
end


% Find the connections seen by the nodes. This computation holds the k-hop
% neighbors of all nodes and their commute times.
node = [];
for i=1:n
   [~, seenByNode] = findConnectGraph(data, i, hop);   
   node = [node; struct('hop',seenByNode)];   
end

save('SyntheticDataset', 'data', 'node', 'hop'); % Save the dataset
fprintf('\nGenerated %g-dimensional dataset of size %g and having %g edges\n',...
    d, n, volume);
clear