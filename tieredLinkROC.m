function [avgAUC, auc, truePos, falsePos] = tieredLinkROC(data, model)

% measure area under ROC curve for link prediction based on
% distance


X = data.X;
A = data.A;
n = data.N;
M = model.params.M;
maxDepth = model.params.maxDepth;


if n > 11000
    fprintf('The %d x %d distance matrix is probably too big to compute. Aborting...\n', n, n);
    return;
end

K = X'*M*X;
XX = diag(K);

D = full(XX*ones(1,n) + ones(n, 1)*XX' - 2*K);

truePos = cell(maxDepth,1);
falsePos = cell(maxDepth,1);
auc = nan(maxDepth, n);

A = A - sparse(1:n, 1:n, diag(A)) > 0;

Ahop = A;
% make self-links infinity distance apart
D = D + sparse(1:n, 1:n, inf(n,1));

[~, distanceOrder] = sort(D, 1, 'ascend');

for i = 1:maxDepth
    
    if i > 1
        % compute i-hop neighbors
        Ahop = (double(Ahop) * double(A) > 0) | Ahop;
        % remove diagonal entries
        Ahop = logical(double(Ahop) - sparse(1:n, 1:n, diag(Ahop)));
    end
    
    % fast version
    for j = 1:n
        if any(Ahop(:,j)) % nodes that have at least one neighbor
            [auc(i,j), falsePos{i,j}, truePos{i,j}] = AUROC(distanceOrder(:,j), Ahop(:,j));
        end
    end
    fprintf('Computed average AUC for %d-hop neighbors: %f\n', i, nanmean(auc(i,:)));        
    
end

avgAUC = nanmean(auc(:));