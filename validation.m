function bestParams = validation(data, method, choice, params)

numCrossVal = 5; %5-fold cross validation

switch(choice)
    case 1
        scorer = @validAUC;
        validType = 'AUC score';
        mult = -1;
    case 2
        scorer = @validSpearman;
        validType = 'Spearmans footrule score';
        mult = 1;
    case 3

    otherwise

end


%% Load from the data
X = data.X;
A = data.A;
N = data.N;
dim = data.dim;
%% Perform k-fold cross-validation to tune the parameter lambda
indices = crossvalind('Kfold', N, numCrossVal);
lambdaVec = [1; 2; 5] * power(10, -6:2);
lambdaVec = lambdaVec(:);
bestScore = inf;

for s = 1:length(lambdaVec)
    % set the parameter 'lambda'
    params.lambda = lambdaVec(s); 
    score = 0;
    
    for k = 1:numCrossVal

        validInds = (indices == k);
        trainInds = ~validInds;

        XvTr = X(:, trainInds);
        AvTr = A(trainInds, trainInds);
        XvTe = X(:, validInds);
        AvTe = A(validInds, validInds);

        nTrain = nnz(trainInds);

        trainData = struct('X', XvTr, 'A', AvTr, 'N', nTrain, 'dim', dim);
        testData = struct('X', XvTe, 'A', AvTe, 'N', N - nTrain, 'dim', dim);

        model = method(trainData, params);
        score = score + feval(scorer, testData, model);

    end
    
    % Report average score
    score = score/numCrossVal;
    fprintf('Training done with lambda: %f. Average %s after %g-fold cross-validation : %f\n',...
        params.lambda, numCrossVal, score, validType);
    % Update the model corresponding to the best score
    if(mult*score<bestScore)
        bestParams = params;
        bestScore = score * mult;
    end    
    
end

bestScore = bestScore * mult;
fprintf('The %s for the best model obtained for lambda %d is %g\n',...
        validType, bestParams.lambda, bestScore);
%{
semilogx(lambdaVec, scores);
xlabel('lambda');
ylabel('validation score');
drawnow;

% find best score (set to min for now)
[~, i] = max(scores);

% find minimum loss (set to min for now)
[~, i] = min(scores);
params.lambda = lambdaVec(i);

%}











function score = validAUC(data, model)
%VALIDAUC Computes the AUC scores of the nodes
%   Detailed explanation goes here

scorer = @tieredLinkROC;
score = scorer(data, model);
    











function score = validSpearman(data, model)
%VALIDSPEARMAN Computes the Spearman's foot-rule score of the nodes
%   Detailed explanation goes here


scorer = @compareDistances;
score = scorer(data, model);
       
