function D = computeMetricDistance(X, M)

K = X' * M * X;

n = size(X,2);

D = (diag(K) * ones(1,n) + ones(n,1) * diag(K)' - 2 * K);
