function M = psdProject(M0, params)
%PSDPROJECT  Projecting the Mahalanobis matrix M into the cone of the PSD matrices
%   Detailed explanation goes here


% if diagonal, just delete negative entries
if(~isempty(params))
    if params.diagonal
        M = M0;
        M(M<0) = 0;
        return;
    end
end

% otherwise use eig to project to PSD
M = false;
k = min(size(M0,1), 100);

while ~M
    % returns a diagonal matrix D contains k largest magnitude eigenvalues
    % of (M0 + M0')/2 and a matrix V whose columns are the corresponding
    % eigenvectors.
    [V,D] = eigs((M0 + M0')/2,k);
    
    if min(diag(D))<=0 || k >= size(M,1)
        inds = diag(D)>0;
        M = V(:,inds)*D(inds,inds)*V(:,inds)';
    else
        k = k*2;
    end
end

end

