function [auc, truePos, falsePos] = AUROC(ranking, labels)

% Treat this as a binary classfication problem
tpCount = full(cumsum(labels(ranking)));
fpCount = (1:numel(labels))' - tpCount;

% trim to only the true positive points and append the minimum threshold
% truePos = [tpCount(labels(ranking)) / nnz(labels); 1];
% falsePos = [fpCount(labels(ranking)) / (numel(labels) - nnz(labels)); 1];

truePos = [tpCount / nnz(labels); 1];
falsePos = [fpCount / (numel(labels) - nnz(labels)); 1];


% compute area of trapezoids between points
heights = 0.5 * (truePos(1:end-1) + truePos(2:end));
bases = falsePos(2:end) - falsePos(1:end-1);
auc = heights'*bases;
