function lossScore = lossFunc(data, M)
%LOSSFUNC Summary of this function goes here
%   Detailed explanation goes here

global evalTriplets
numTrip = size(evalTriplets,1); 
X = data.X;

lossScore = 0;

for t = 1:numTrip
    i = evalTriplets(t, 1);
    j = evalTriplets(t, 2);
    k = evalTriplets(t, 3);
    Xi = X(:, i);
    Xj = X(:, j);
    Xk = X(:, k);
    
    %% faster kernel value computation
    XiM = (M * Xi)';
    XjM = (M * Xj)';
    XkM = (M * Xk)'; 
    Kii = XiM * Xi;
    Kjj = XjM * Xj;
    Kkk = XkM * Xk;
    Kij = XiM * Xj;
    Kji = Kij;
    Kik = XiM * Xk;
    Kki = Kik;
    
    distk = Kii + Kkk - Kik - Kki;
    distj = Kii + Kjj - Kij - Kji;
    %% computing the loss function 
    lossScore = lossScore + max(0, distj - distk + 1);
end
lossScore = lossScore/numTrip;

% lossScore = lossScore + 0.5 * lambda * M(:)' * M(:);
