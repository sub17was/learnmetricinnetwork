function score = stochInvSampling(K)
%STOCHINVSAMPLING Summary of this function goes here
%   Detailed explanation goes here

global paths

%{
X = data.X;
A = data.A;
M = model.M;
K = X' * M * X;
%}
% Number of nodes

numPaths = length(paths);


% Computes the sum of number of impostors for all nodes
score = 0;


for i=1:length(paths)
    
    path = paths{i,1};
    
    % Reference node
    idx = path(1);
    
    % Remaining nodes
    remNodes = path(2:end);
    pathLength = length(remNodes);
    
    metricDistances = K(idx, remNodes);
    
    
% compute average Spearman's footrule between distance rankings
    
    [~, metricRank] = sort(metricDistances, 'ascend');
    graphRank = 1:pathLength;
%     t = ceil(pathLength/2);
%     score = score + sum(abs(graphRank - metricRank)) / (t * (1 + t/2));
    score = score + sum(abs(graphRank - metricRank));
end

score = score / numPaths;