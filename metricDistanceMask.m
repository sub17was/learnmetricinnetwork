function D = metricDistanceMask(X, M, mask)

% Computes the distance matrix of X only in nonzeros of mask

% Number of elements in the matrix
n = size(X,2);

XM = X' * M;

if nargin==3 && issparse(mask)
    % If you can't compile the mex file spouterprod2, use the matlab
    % version spouterprod.m
    K = spouterprod( mask, XM, X' ) + mask;
%     K = spouterprod2( mask, sparse(XM'), sparse(X) ) + mask;
else
    K = XM * X + mask;
end


XX = sum(XM' .* X)';

% Stores the row and column indices in I and J respectively, corresponding
% to the non-zero elements in masks
[I,J] = find(mask);

% Sums the distance values stored in the non-zero entries of the mask
V = XX(I) + XX(J);

if (nnz(mask) > nnz(K))
    fprintf('Something is wrong\n');
else
    V = V - 2*( nonzeros(K) - 1 );
    clear K;
end

[I,J] = find(mask);

D = sparse(I,J,V,n,n);

end
