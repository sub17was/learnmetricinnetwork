function data = getData(choice, varargin)
%GETDATA Summary of this function goes here
%   Loads the artificial datasets for testing
% 

switch(choice)
    case 1
        funcname = @Wikipedia;        
    case 2
        funcname = @Citation;
    case 3
        funcname = @Facebook;
    case 4
        funcname = @GraphTheory;
    case 5
        funcname = @PhilConcepts;
    case 6
        funcname = @SearchEngines;

end

if(nargin > 1) % supplied for synthetic data
    data = feval(funcname, varargin); 
else
    data = feval(funcname);
end
% remove isolated nodes from the graph
data = remIsolatedNodes(data);

% find the largest connected component of the graph starting from the node
% with the highest degree
data = findConnectGraph(data);

X = data.X; 
N = data.N;

% normalize data
% X = bsxfun(@rdivide, X, sum(X, 1));
X = bsxfun(@rdivide, X, max(abs(X),[],2)); % Code to normalize features
X(isnan(X)) = 0;

% make A symmetric
A = data.A & (data.A)';
% remove self-links
A = A - sparse(1:N, 1:N, diag(A)) > 0;

% update with the processed data
data.X = sparse(X); % Sparse Representation
data.A = sparse(A);


end










function data = Wikipedia()

load data/WikiBFSDump100000;
fprintf('\n Wikipedia dataset \n');
N = size(A,1);
d = sum(size(X)) - N;

data = struct('X',X','A',A,'dim',d,'N',N);
end











function data = Citation()

load data/citationData;

y = randi([1992 2003]) - 1991;

A = citeData(y).link;
X = citeData(y).attribute;
fprintf('\n Citation dataset for the year %d\n',citeData(y).year);
N = size(A,1);
d = sum(size(X)) - N;

data = struct('X', X, 'A', A, 'dim', d, 'N', N, 'year', citeData(y).year);
clear citeData;
end










function data = Facebook()


fprintf('\n Facebook dataset \n');
load('facebook.mat');
[d,N] = size(X);
data = struct('X', X, 'A', A, 'dim', d, 'N', N);

end







function data = GraphTheory()
%CREATESYNTHETICDATA Summary of this function goes here
%   Detailed explanation goes here

fprintf('\n Graph Theory dataset \n');
load('graphTheory.mat');
[n, d] = size(X);

data = struct('X', X', 'A', A, 'dim', d, 'N', n);

end









function data = PhilConcepts()
%CREATESYNTHETICDATA Summary of this function goes here
%   Detailed explanation goes here

fprintf('\n Philosophy Concepts dataset \n');
load('philConcepts.mat');
[n, d] = size(X);

data = struct('X', X', 'A', A, 'dim', d, 'N', n);

end






function data = SearchEngines()
%CREATESYNTHETICDATA Summary of this function goes here
%   Detailed explanation goes here

fprintf('\n Search Engines dataset \n');
load('searchEngines.mat');
[n, d] = size(X);

data = struct('X', X', 'A', A, 'dim', d, 'N', n);

end






function truncData = findConnectGraph(data)
%FINDCONNECTGRAPH Summary of this function goes here
%   This function finds the largest connected component of a graph using
%   the breadth-fast traversal technique.


A = data.A;
X = data.X;
N = size(A,1); % Number of nodes in the graph

[~, highDegNode] = max(sum(A,1)); % The most-well connected node of the graph


%% Start the breadth-fast traversal from the most well-connected node. 
% The code goes upto 6-hop neighbors. You can modify it by changing the
% variable 'hop'
hop = 6;
visited = cell(1+hop,1); %Holds the varying hop neighbors
hopLevel = 1;
visited{hopLevel,1} = highDegNode;
% This is a reverse flag that is false corresponding to all nodes checked.
visitInd = true(N,1);
visitInd(highDegNode) = false;



while(hopLevel<=hop)
    hopLevel = hopLevel + 1;
    tempVisitInd = false(N,1);
    prevLevel = visited{hopLevel-1,1};
    for i = 1:numel(prevLevel)
        tempVisitInd = tempVisitInd | (A(:,prevLevel(i)) & visitInd);
    end
    visited{hopLevel,1} = find(tempVisitInd);
    visitInd(visited{hopLevel,1}) = false;    

end

% Truncate the Adjacency matrix A and the attribute matrix X so that only
% the connected components remain

%{
% Sanity check to see if the algorithm is working correctly. To implement
% this truncate the data to only 2 attributes
figure(100);
gplot(A, X', 'k*-.');
Atrunc = A(~visitInd, ~visitInd);
Xtrunc = X(1:2, ~visitInd);
figure(101);
gplot(Atrunc, Xtrunc', 'bv-');
%}
truncData = data;
truncData.X = X(:, ~visitInd);
truncData.A = A(~visitInd, ~visitInd);
truncData.N = size(truncData.A,1);

end


function data = remIsolatedNodes(data)
%REMOVISONODES Removes isolated nodes from the dataset
%   Detailed explanation goes here

A = data.A;
X = data.X;
I = find(sum(A));
data.A = A(I,I);
data.X = X(:,I);
data.N = size(data.A,1);

end