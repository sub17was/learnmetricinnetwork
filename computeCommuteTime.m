function H = computeCommuteTime(A)

% Computes the commute time for a symmetric graph encoded in adjacency
% matrix A

% keeps a cache of the last-used A

persistent savedA
persistent savedLinv;


n = size(A,1);

if ~isempty(savedA) && all(size(savedA) == size(A)) && all(savedA(:) == A(:))
    Linv = savedLinv;
else
    % Compute the Laplacian matrix of the graph
    
    A = full(A);
    d = sum(A,2);
    D = diag(d);
    L = D - A;
    
    Linv = pinv(L); % pseudo-inverse of graph Laplacian    
    
    savedA = A;
    savedLinv = Linv;
end

Ldiag = diag(Linv);

volume = sum(A(:));

H = volume * (Ldiag * ones(1,n) + ones(n,1) * Ldiag' - Linv - Linv');

% classical formula as written in related work of 
% http://arxiv.org/pdf/1104.3791v1.pdf