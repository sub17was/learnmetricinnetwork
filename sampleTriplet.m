function triplets = sampleTriplet(data, numTriplets, choice)
%PRESAMPTRIP This function presample triplets for empirical testing
%   Detailed explanation goes here

if ~isfield(data, 'F')
    if(choice<3)
        data.F = data.A;
    elseif(choice<4)
        data.F = computeHittingTime(data.A);
    elseif(choice<6)
        data.F = computeCommuteTime(data.A);
    end
end

switch(choice)
    case 1
        tripfunc = @sampleRandTriplet;
    case 2
        tripfunc = @sampleRandWalkTriplet;
    case 3
        tripfunc = @sampleDegTriplet;
    case 4
        tripfunc = @sampleMeasureTriplet;
    case 5
        tripfunc = @sampleCtspmlTriplet;
end
    
triplets = tripfunc(data, numTriplets);






















function triplet = sampleRandTriplet(data, numTriplets)
% Randomly choose a node and set it as the reference point for getting the
% target impostor' and 'farthest neighbor'

A = data.A;
N = data.N;
count = 0;
triplet = zeros(numTriplets,3);

while(count<numTriplets)
% Randomly choose a node and set it as the reference point for
% getting the 'target impostor' and 'farthest neighbor'
i = randi(N);

% The variable delta ensures that w.r.t. to node 'i' we do not
% select 'i' as the disconnected node, since A(i,i) = 0
delta = zeros(1, N);
delta(i) = 1;

% Find the indices of nodes that are connected to it using the
% adjacency matrix A
conIDX = find(A(:,i) == 1);

% If it is connected to at least one node (and not itself) we
% will proceed with further computation
  if ~isempty(conIDX) && length(conIDX) < N
    
    idx = randi(length(conIDX));
    j = conIDX(idx);
    
    % If there are lot of disconencted nodes it is easy to find one
    % using random sampling
    if length(conIDX) < N/100
        k = randi(N);
        while ismember(k, conIDX)
            k = randi(N);
        end
        % If there are not too many disconnected nodes then we use the
        % adjacency matrix A and the find() function to get a
        % disconnected node randomly.
    else
        disIDX = find((A(i, :) + delta) == 0); %remove O(n) time
        idx = randi(length(disIDX));
        k = disIDX(idx);
    end
    
    count = count + 1;
    triplet(count,:) = [i j k]; % So that A(i,j)=1 and A(i,k)=0    
  end
  
end






function triplet = sampleRandWalkTriplet(data, numTriplets)
%   Sample nodes 'j' and 'k' such that 'j' is from a shorter path than 'k'

A = data.A;
N = data.N;

count = 0;
triplet = zeros(numTriplets,3);

while(count<numTriplets)
    rejectFlag = true;
    params = struct(); % set random walk parameters here. For now use defaults

    while(rejectFlag)

        nodeI = randi(N);
        rejectFlag = ~any(A(:,nodeI)); % true if it is an isolated node

        if(~rejectFlag)
            [visitedIndsA, ~] = absorbingRandomWalk(A, nodeI, params);
            [visitedIndsB, ~] = absorbingRandomWalk(A, nodeI, params);

            if length(visitedIndsA) < length(visitedIndsB)
                nodeJ = visitedIndsA(end);
                nodeK = visitedIndsB(end);
            elseif length(visitedIndsB) < length(visitedIndsA)
                nodeJ = visitedIndsB(end);
                nodeK = visitedIndsA(end);
            else % If both random walk paths have the same length
                rejectFlag = true;
            end
        end
    end
    count = count + 1;
    triplet(count,:) = [nodeI nodeJ nodeK];
end






function [visitedList, status, visited] = absorbingRandomWalk(data, i, params)


A = data.A;
N = data.N;

%% get random walk parameters 
if isfield(params, 'absorpProb')
    absorpProb = params.absorpProb;
else
    absorpProb = 0.2;
end

if isfield(params, 'teleportProb')
    teleportProb = params.teleportProb;
else
    teleportProb = 0.01;
end


neighbors = (A(:,i) == 1); % neighbors of node i
    
neighborInds = find(neighbors); % index of neighbors of node i
walkCount = 1;
current = neighborInds(randi(nnz(neighbors)));
      
visitedList = zeros(N, 1);
status = zeros(N, 1);
visited = sparse([i current], [1, 1], true(2,1), N, 1);
teleportCount = 1; % To judge the levels of teleportation
visitedList(1) = current;
status(1) = teleportCount;

while((rand>absorpProb) || (walkCount<3))
    neighbors = (A(:,current) == 1);
    neighbors(neighbors & visited) = false;
    
    if(~any(neighbors) || rand<teleportProb) % teleport to a node 
        flag = true;  
        while(flag) % select a random node that is yet to be visited
            current = randi(N);
            flag = visited(current);
        end
        
        visited(current) = true;          
        teleportCount = teleportCount + 1;   % number of teleportations made            
    else % continue further in the walk
        
        neighborInds = find(neighbors);
        current = neighborInds(randi(nnz(neighbors)));
        visited(current) = true;
    end
    
    walkCount = walkCount + 1;
    visitedList(walkCount) = current;
    status(walkCount) = teleportCount;
end

visitedList = visitedList(1:walkCount);
status = status(1:walkCount);
        


















function triplet = sampleDegTriplet(data, numTriplets)
% Samples triplets such that node j has more degrees than node k

D = data.F;
N = data.N;

count = 0;
triplet = zeros(numTriplets,3);

while(count<numTriplets)
    % Randomly choose 3 nodes
    i = randi(N);
    j = randi(N);
    k = randi(N);    
    
    if i ~= j && j ~= k && i ~= k && (D(j) ~= D(k))
        count = count + 1;
        if D(j) > D(k)
            triplet(count,:) = [i j k];
        elseif D(j) < D(k)
            triplet(count,:) = [i k j];
        else
            count = count - 1;
            continue
        end
        
        assert(D(triplet(count, 2)) > D(triplet(count, 3)));
        
    end
end












function triplet = sampleMeasureTriplet(data, numTriplets)
%   Performs sampling based on path-based measures suuplied to it.
%   Path-based measures include commute-time, Katz measure, etc.


C = data.F; % This is the custom-supplied measure
N = data.N;

count = 0;
triplet = zeros(numTriplets,3);

% Sample triplets such that the measure between (i,j) is less than the
% measure between (i,k)
while(count<numTriplets)
    % Randomly choose 3 nodes
    i = randi(N);
    j = randi(N);
    k = randi(N);
    
    
    if i ~= j && j ~= k && i ~= k && (C(i,j) ~= C(i,k))
        count = count + 1;        
        if C(i,k) < C(i,j)
            triplet(count,:) = [i k j];
        elseif C(i,j) < C(i,k)
            triplet(count,:) = [i j k];
        else
            count = count - 1;
            continue
        end
        
        assert(C(triplet(count, 1), triplet(count, 2)) < C(triplet(count, 1), triplet(count, 3)));
        
    end
end












function triplet = sampleCtspmlTriplet(data, numTriplets)
% Randomly choose a node and set it as the reference point for getting the
% target impostor' and 'farthest neighbor'

A = data.A;
N = data.N;
C = data.F;

count = 0;
triplet = zeros(numTriplets,3);

while(count<numTriplets)
% Randomly choose a node and set it as the reference point for
% getting the 'target impostor' and 'farthest neighbor'
i = randi(N);

% The variable delta ensures that w.r.t. to node 'i' we do not
% select 'i' as the disconnected node, since A(i,i) = 0
delta = zeros(1, N);
delta(i) = 1;

% Find the indices of nodes that are connected to it using the
% adjacency matrix A
conIDX = find(A(:,i) == 1);

% If it is connected to at least one node (and not itself) we
% will proceed with further computation
  if ~isempty(conIDX) && length(conIDX) < N
    
    idx = randi(length(conIDX));
    j = conIDX(idx);
    % Maximum commute time among the connected neighbors
    maxConCommuteTime = max(C(conIDX,i));
    
    disIDX = find((A(i, :) + delta) == 0); %O(n) time
    
    % If there are lot of disconencted nodes it is easy to find one
    % using random sampling
    if(0 < length(disIDX) && length(disIDX) < N)
        % Find the non-neighbors that have less commute time than
        % the maximum of commute times of connected neighbors
        disconComTimeIDX = disIDX(C(disIDX,i)<maxConCommuteTime);
        % If such non-neighbors exist take one of them to be your 'node k'
        if(~isempty(disconComTimeIDX))
            k = disconComTimeIDX(randi(length(disconComTimeIDX)));
            count = count + 1;
            triplet(count,:) = [i j k]; % So that A(i,j)=1 and A(i,k)=0 
        end        
    end
  end
  
end
