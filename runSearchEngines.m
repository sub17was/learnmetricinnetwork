%% This file does a sanityCheck to see what evaluation metric we need to
% use for our work. We will be training and testing on the same dataset.
% This confirms the metric to be used. Then we will also see the number of
% inversions alongside. In expectation, the right metric should also aim
% to reduce the numeber of inversions on the dataset.
%  The metrics used for comparison are
% 1. average AUC scores of all the nodes on k-hop neighbors
% 2. Spearman's footrule measure

clear all
close all


rng(1); % seed 1 given to the random number generator

%% Define the methods that will be compared
methods = {@methodEuclidean, @methodLEGOtriplet};%@methodCTML, @methodKMML, @methodLEGO, @methodSPML};
methodName = {'Euclidean', 'LEGOtriplet'};%, 'KMML', 'LEGO', 'SPML'};
numMethods = length(methods); % number of test methods considered

% Add the path of the data files and the methods for generating them
% addpath ../data ../data/Laplacian

%% Load data-set to test the metrics

% ARTIFICIAL dataset with commute-time embeddings
% load('SyntheticDataset.mat') 
%OR Comment previous line and uncomment next line 

% If you want to load data from dataset make a call to the function getData
% data = getData(choice, varargin)
% choice Dataset   
%     1 Wikipedia      
%     2 Citation
%     3 Facebook
%     4 GraphTheory
%     5 PhilConcepts
%     6 SearchEngines
data = getData(6);
%}

%% Duplicate the data to prevent accidental alteration
dim = data.dim;
N = data.N;
W = eye(dim); % If you want to use the data set as it is
% W = rand(dim); % If you want to scramble the datset
X = W*data.X;    % The datset obtained by rotating and scaling X
A = data.A;


%% Different values of lambda used for simualtion
lambdaVec = power(10, -6:4);
lambdaVec = lambdaVec(:);
numParams = length(lambdaVec);

%% Define the metrics for assessing the demo run
% savedParams = cell(numMethods, numParams);
% valScores = cell(numMethods,1);
avgAUC = nan(numMethods, numParams);    % average AUC scores till depth D
auc = cell(numMethods, numParams);      % auc scores for the dataset
truePos = cell(numMethods, numParams);  % true positive rate
falsePos = cell(numMethods, numParams); % false positive rate
model = cell(numMethods, numParams);    % model used to test run
% computeTime = zeros(numMethods, numParams); % time taken for the method to run
spearScore = zeros(numMethods, numParams); % holds the average score for Spearman's footrule
stochInvScore = zeros(numMethods, numParams);

%% Run the experiment on different models
for m = 1:numMethods
    fprintf('\n%s\n',methodName{m});
    % Parameter tuning
    method = methods{m};
    params = checkParams(data, struct());

    % Training and Validation
    for s = 1:numParams
        fprintf('Training for lambda %f\n',lambdaVec(s));        
        params.lambda = lambdaVec(s); % set the parameter 'lambda'
%         tstart = tic; % start timer
        model{m,s} = method(data, params); % store the model
%         computeTime(m,s) = toc(tstart); % stop timer and record time
    end
end


% Uncomment next two lines if you want to run the stochastic inversion
% sampling (line 105-109)
% global paths
% paths = samplePaths( data.A, [] );
%% On the training data, evaluate the different scores for the different models
for m = 1:numMethods
    fprintf('\n%s\n',methodName{m});
    fprintf('\n Initial number of impostors: %g\n',model{m,s}.initialNumImpostors);
 
    if(m>1) % If not Euclidean
        limit = numParams;
    else
        limit = 1; % If Euclidean just one run is fine
    end
    
    for s = 1:limit
        if(m>1)
            fprintf('\nLambda : %f\n',model{m,s}.params.lambda)
        end
       
        % Computing the average AUC, AUC, True Positive and False
        % positive rate
        [avgAUC(m,s), auc{m,s}, truePos{m,s}, falsePos{m,s}] = tieredLinkROC(data, model{m,s}); 
        fprintf('Average AUC score %f\n\n', avgAUC(m,s));
        %}
        % Print the Spearman's footrule
        fprintf('Initial Spearman''s foot-rule score %f\n', model{m,s}.initialSpearScore);
        fprintf('Final Spearman''s foot-rule score %f\n', model{m,s}.finalSpearScore);        
        % Print final number of impostors
%         model{m,s}.afterNumImpostors = countNumImpostors(data, model{m,s});
        fprintf('Final number of impostors: %g\n',model{m,s}.afterNumImpostors);        
%         fprintf('Norm of the metric M %f\n', norm(full(model{m,s}.params.M),2));
        %{ 
        % This code is commented since it requires more testing
        %  Compute the Stochastic Inversion sampling
        M = model{m,s}.params.M;
        model{m,s}.stochInvScore = stochInvSampling(X' * M * X);
        fprintf('\n Stochastic Inversion score : %g\n', model{m,s}.stochInvScore);
        %}
    end   
end


%% Storing the diagnostics for the run
save('searchEnginesStat.mat', 'model', 'data', '-v7.3');