function [X,A] = preferential(varargin)

N = varargin{2};
n0 = varargin{1}*2;

theta = linspace(0, 2*pi - 2*pi/n0, n0);

X = zeros(N,2);
X(1:n0,1) = cos(theta);
X(1:n0,2) = sin(theta);

A = zeros(N);
A(1:n0, 1:n0) = 1;
A = A.*(1-eye(N));

for i = n0+1:N
    deg = sum(A,2);
    
    cdf = cumsum(deg(1:i));
    
    seed = rand * cdf(end);
    
    neighbor = find(seed <= cdf, 1, 'first');
    
    A(i, neighbor) = 1;
    A(neighbor, i) = 1;
    
    X(i,1:2) = randn(1,2);
    %{
    gplot(A, X(:,1:2), '.-');
    drawnow;
    %}
end
