function [] = plotDistanceTiers(X, A, M, graphDistanceComputer, maxPlotDistance)

% graphDistance is a struct containing the computed distances and indexes
% of tiers of distance

%% Compute graph distance from one node

[~, centralNode] = max(sum(A));

graphDistance = graphDistanceComputer(A, centralNode);

distance = graphDistance.distance;
distanceIndex = graphDistance.distanceIndex;
distanceBins = min(17, length(distanceIndex) - 1);

%% Compute attribute distances from node

metricDistances = full(sqrt(X(:, centralNode)'*M*X(:, centralNode) - 2*(X(:, centralNode)'*M*X)' + sum((X'*M)'.*X)'));

%% plot distance tiers
% TODO: refactor this to run for general inputs
plotInds = distance > 0 & (distance <= maxPlotDistance);
bh = boxplot(metricDistances(plotInds), distance(plotInds), 'whisker', 0);
set(bh,'linewidth',2);
h=findobj(gca,'tag','Outliers');
delete(h)
axis tight;
ax = axis;
axis([ax(1)-0.5, ax(2)+0.5, ax(3)-10, ax(4)+10]);

meds = zeros(distanceBins, 1);
for i = 2:distanceBins+1
    meds(i-1) = median(metricDistances(distanceIndex{i}));
end

hold on;
leftEdge = distanceBins - 0.75;
rightEdge = distanceBins - 0.25;
plot([(1.25:1:leftEdge); (1.75:1:rightEdge)], [meds(1:end-1)'; meds(2:end)'], 'k-', 'LineWidth', 1, 'MarkerSize', 8);
hold off;

set(gca, 'FontSize', 12);
xlabel('Shortest Path Distance', 'FontSize', 12);
ylabel('Attribute Distance', 'FontSize', 12);
