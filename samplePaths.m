function paths = samplePaths( A, params )
%SAMPLEPATHS Summary of this function goes here
%   Detailed explanation goes here

%% get random walk parameters 
if isfield(params, 'absorpProb')
    absorpProb = params.absorpProb;
else
    absorpProb = 0.2;
end


N = size(A,1);

if isfield(params, 'numPaths')
    numPaths = params.numPaths;
else
    numPaths = ceil(N*N/2);
end

paths = cell(numPaths, 1);
    
% Count how many paths computed
count = 0;
t=[];

while(count<numPaths)
    flag = true;
    
    while(flag)
        i = randi(N); % Select a random node
        neighbors = (A(:,i) == 1); % neighbors of node i

        neighborInds = find(neighbors);
        flag = isempty(neighborInds); % If the node is empty select another one
    end
    
    walkCount = 1;
    current = neighborInds(randi(length(neighborInds)));

    visited = sparse([i current], [1, 1], true(2,1), N, 1);
    path = [i; current];
    
    while((rand>absorpProb) || (walkCount<3))
        
        neighbors = (A(:,current) == 1);
        neighborInds = find(neighbors);
        
        if(~isempty(neighborInds))
            current = neighborInds(randi(length(neighborInds)));
            visited(current) = true;
            walkCount = walkCount + 1;
            path = [path; current];
        else % The chain continues no more
            break
        end
    end
    
    count = count + 1;
    t = [t length(path)];
    paths{count, 1} = path;
    
end

% function [paths, status, visited] = absorbingRandomWalk(A, i, params)